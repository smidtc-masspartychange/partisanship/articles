# Past Publications on Stability and Change in Mass Partisanship

2018 - [The Consequences of Elite Party Politics for American Macropartisanship](https://gitlab.com/smidtc-masspartychange/partisanship/articles/blob/master/2018_macrojop_web.pdf) - Journal of Politics
     - [Supplemental](https://gitlab.com/smidtc-masspartychange/partisanship/articles/blob/master/2018_jop_supplemental_appendix.pdf)
     - [Replication](https://dataverse.harvard.edu/dataset.xhtml?persistentId=doi:10.7910/DVN/1GLFE1)

2017 - [Polarization and the Decline of the American Floating Voter](https://gitlab.com/smidtc-masspartychange/partisanship/articles/blob/master/2017_floating_ajps.pdf) - American Journal of Political Science
     - [Supplemental](https://gitlab.com/smidtc-masspartychange/partisanship/articles/blob/master/2017_floating_ajps_SI.pdf)
     - [Replication](https://dataverse.harvard.edu/dataset.xhtml?persistentId=doi:10.7910/DVN/4FCVUR)

2014 - [Dynamics in Partisanship in American Presidential Campaigns](https://gitlab.com/smidtc-masspartychange/partisanship/articles/blob/master/2014_POQ_campaignpid.pdf) - Public Opinion Quarterly
     - [Replication](https://gitlab.com/smidtc-masspartychange/partisanship/replication/tree/master/2014_campaignpid_poq)

2011 - [The Dynamic Properties of Individual-level Party Identification in the United States](https://gitlab.com/smidtc-masspartychange/partisanship/articles/blob/master/2011_ES_dynamicpanel.pdf) - Electoral Studies - with Brandon Bartels, Jan Box-Steffensmeier, and Renee Smith.
     - [Replication logs](https://gitlab.com/smidtc-masspartychange/partisanship/replication/tree/master/2011_electoralstudies_dynamicpanel)
